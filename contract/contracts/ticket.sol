pragma solidity ^0.5.16;

import "ERC721.sol";

contract Stamped is ERC721 {

  string public name;
  uint256 public price;
  uint256 public validity;
  address payable public owner;

  struct Ticket {
    string ticketName;
    uint256 ticketId;
    uint256 ticketValidity;
    uint256 ticketPrice;
  }

  constructor(
      string memory _name,
      uint256 _price,
      uint256 _validity
  ) public {
      name = _name;
      price = _price;
      validity = _validity;
      owner = msg.sender;
  }

  Ticket[] public tickets;

  event ticketPurchase(string _message);


  function getTicket() public payable {
    address payable customer = msg.sender;
    uint amount = msg.value;
    owner.transfer(amount);
    require(amount > price, "you do not have enough money");
    uint256 nbTicket = amount / price;
    uint256 change = amount % price;
    if (change > 0) {
      customer.transfer(change);
    }
    for (uint i=0; i < nbTicket; i ++) {
      createTicket(msg.sender);
    }
  }

  function createTicket(address _to) private {
    string memory ticketName = name;
    uint256 ticketId = getTicketCount() + 1;
    uint256 ticketValidity = now + validity;
    uint256 ticketPrice = price;

    tickets.push(
          Ticket(ticketName, ticketId, ticketValidity, ticketPrice)
    );
    emit ticketPurchase("you have successfully acquired ticket.");
    _mint(_to, ticketId);
  }

  function getTicketCount() public view returns (uint256) {
    return tickets.length;
  }

  function transferTicket(address _to, uint256 _id) public {
    require(tickets.length >= _id, "ID not available.");
    transferFrom(msg.sender, _to, _id);
  }

  function check(uint256 _id) public view returns (bool) {
    require(tickets.length >= _id, "ID not available.");
    if (tickets[_id].ticketValidity < now) {
      return false;
    }
    return true;
  }

  function modifyTicketValue(string memory newName, uint256 newValidity, uint256 newPrice) public {
    require(msg.sender == owner, "Your not the owner of this contract.");
    name = newName;
    validity = newValidity;
    price = newPrice;
  }

}
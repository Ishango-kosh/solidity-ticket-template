/*
const truffle = require("truffle");
if (typeof web3 !== 'undefined') {
    App.web3Provider = web3.currentProvider;
    web3 = new Web3(web3.currentProvider);
} else {
    // If no injected web3 instance is detected, fallback to Ganache.
    App.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:8545');
    web3 = new Web3(App.web3Provider);
}
*/
const provider = new Web3.providers.WebsocketProvider('ws://127.0.0.1:8545');

let request = new XMLHttpRequest();
request.open('GET', 'http://127.0.0.1:3000/api');
request.send();

request.onload = function() {
  if (request.status != 200) {
    console.log(`Error ${request.status}: ${request.statusText}`);
  } else {
    console.log(`Done ${request.status}: ${request.statusText}`);
  }
};

request.onerror = function() {
  console.log('Request failed');
};

const createAnchor = async (hash, state) => {
    const ticketStamp = state.ticketStampContract;
    return await ticketStamp.methods.createAnchor(hash).send({ from: state.accountAddress, gas: state.gasLimit });
}

const hasAnchor = async (targetHash, state) => {
    const ticketStamp = state.ticketStampContract;
    return { [targetHash]: await ticketStamp.methods.hasAnchor(targetHash).call({ from: state.accountAddress }) };
}